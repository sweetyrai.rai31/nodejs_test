const xlsxFile = require('read-excel-file/node')
const fs = require('fs');

var data_array = []
var small_alphabets = "abcdefghijklmnopqrstuvwxyz".split('')
var count = 0

new Promise((resolve, reject) => {
    xlsxFile('./sample.xlsx', {
        getSheets: true
    }).then((blocks) => {
        blocks.forEach(async function (obj, ind) {

            var new_obj = {}
            new_obj.name = obj.name
            new_obj.data = []

            await xlsxFile('./sample.xlsx', {
                sheet: obj.name
            }).then(async function (rows) {
                count++
                new Promise((innerResolve, innerReject) => {
                    rows.forEach((row, i) => {
                        var inner_obj = {}
                        var data = Object.assign({}, row)
                        inner_obj[small_alphabets[i]] = data

                        new_obj.data.push(inner_obj)
                        innerResolve(new_obj)
                    })
                }).then(resp => {
                    if (blocks.length == count) {
                        resolve(data_array)
                    }
                    data_array.push(resp)

                }).catch(err => {
                    console.log(err);
                })

            })

        })

    })
}).then(response => {
    var json = JSON.stringify(data_array);
    fs.writeFile('myjsonfile.json', json, 'utf8', function (response) {
        console.log('File created successfully')
    })
}).catch(error => {
    console.log(error);
})