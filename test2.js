var express = require("express");
var app = express();
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false,
}))

var port = 3000
app.listen(port, () => {
    console.log("Server running on port " + port);
});

app.post("/test", (req, res) => {
    var data = req.body
    var new_data = {}
    for (var [key, value, ind] of Object.entries(data)) {
        if (typeof value != 'object') {
            new_data[key] = typeof value
            data[key] = typeof value

        } else {
            if (Array.isArray(value)) {
                value.map(val => {
                    for (const [k, v] of Object.entries(val)) {
                        val[k] = typeof v
                    }
                })

            } else {
                for (const [k, v] of Object.entries(value)) {
                    value[k] = typeof v
                }
            }

        }
    }
    res.send(data);
});